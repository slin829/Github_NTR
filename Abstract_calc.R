## Abstract calculation
 # OHC loss

abscalc <- function(treatment, i1, i2,cell.type){
  source("plotNTR.R")
  library("Hmisc")
  s1 = i1/5
  s2= i2/5
  plotNTR(treatment, cell.type) # make sure ave_cochlgm.R reads from cellsur.csv
  if(cell.type == "IHC"){
    x <- result.summary$normIHC
  }else if(cell.type == "OHC"){
    x <- result.summary$normOHC
  }
  x <- as.data.frame(x)
  Section=seq(5,100,by=5)
  sur <- cbind(Section,x)
  sur.data <- vector()
  for(i in s1:s2){
    result <- sur[i,]
    sur.data <- rbind(sur.data, result)
  }
 # sur.data$"average"<- NULL
#  sur.data$"std"<- NULL
#  sur.data$"n"<- NULL
#  sur.data$"sem"<- NULL

  sur.data$"Section"<- NULL
  
  #collaps data
  len=length(sur.data)
  dat <- vector("list")
  for(i in 1:len){
    dat[[i]] <- sur.data[,i]
  }
  Data<<-unlist(dat)
  
  ave = mean(Data,na.rm=TRUE)
  sd = sd(Data,na.rm=TRUE)
  n = sum(!is.na(Data))
  n_section = len
  sem <- sd/sqrt(n)
  result.intensity <- rbind(ave,n_section, sd, sem)
  colnames(result.intensity) <- treatment

  print(result.intensity)
}