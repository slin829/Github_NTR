barplotDF <- function(treatment, cell.type){
  data <- read.csv(paste0(treatment, ".csv"))
  cochlea <- names(table(data$Cochlea.Number))
  cochlea
  apical <- subset(data, data$Section %in% c(paste0("S",seq(1,6))))
  middle <- subset(data, data$Section %in% c(paste0("S",seq(7,14))))
  basal <- subset(data, data$Section %in% c(paste0("S",seq(15,20))))
  
  Apical_IHC <- vector()
  Apical_OHC <- vector()
  for(c in cochlea){
    df <- subset(apical, apical$Cochlea.Number %in% c)
    ave_IHC <- mean(df$IHC-df$Background, na.rm=T)
    ave_OHC <- mean(df$OHC-df$Background, na.rm=T)
    n_IHC <- sum(!is.na(df$IHC))
    n_OHC <- sum(!is.na(df$OHC))
    IHC.result <- data.frame(cochlea=c, turn="apical", treatment, ave= ave_IHC,  n=n_IHC)
    OHC.result <- data.frame(cochlea=c, turn="apical", treatment, ave= ave_OHC, n=n_OHC)
    Apical_IHC <- rbind(Apical_IHC, IHC.result)
    Apical_OHC <- rbind(Apical_OHC, OHC.result)
  }
  Middle_IHC <- vector()
  Middle_OHC <- vector()
  for(c in cochlea){
    df <- subset(middle, middle$Cochlea.Number %in% c)
    ave_IHC <- mean(df$IHC-df$Background, na.rm=T)
    ave_OHC <- mean(df$OHC-df$Background, na.rm=T)
    n_IHC <- sum(!is.na(df$IHC))
    n_OHC <- sum(!is.na(df$OHC))
    IHC.result <- data.frame(cochlea=c, turn="middle", treatment, ave= ave_IHC,  n=n_IHC)
    OHC.result <- data.frame(cochlea=c, turn="middle", treatment, ave= ave_OHC, n=n_OHC)
    Middle_IHC <- rbind(Middle_IHC, IHC.result)
    Middle_OHC <- rbind(Middle_OHC, OHC.result)
  }
  Basal_IHC <- vector()
  Basal_OHC <- vector()
  for(c in cochlea){
    df <- subset(basal, basal$Cochlea.Number %in% c)
    ave_IHC <- mean(df$IHC-df$Background, na.rm=T)
    ave_OHC <- mean(df$OHC-df$Background, na.rm=T)
    n_IHC <- sum(!is.na(df$IHC))
    n_OHC <- sum(!is.na(df$OHC))
    IHC.result <- data.frame(cochlea=c, turn="basal", treatment, ave= ave_IHC,  n=n_IHC)
    OHC.result <- data.frame(cochlea=c, turn="basal", treatment, ave= ave_OHC, n=n_OHC)
    Basal_IHC <- rbind(Basal_IHC, IHC.result)
    Basal_OHC <- rbind(Basal_OHC, OHC.result)
  }
  
  ## -------------combine dataframe--------------------
  ##--------------------------------------------------
  if(cell.type =="IHC"){
    df_IHC <- rbind(Apical_IHC, Middle_IHC, Basal_IHC)
    summary_IHC <- vector()
    for(turn in c("apical", "middle", "basal")){
      df <- subset(df_IHC$ave, df_IHC$turn %in% turn)
      ave <- mean(df, na.rm=T)
      sd <- sd(df, na.rm=T)
      n= sum(!is.na(df))
      sem = sd/sqrt(n)
      result <- data.frame(turn, treatment, cell.type = "IHC", ave, sd, n,sem)
      summary_IHC <- rbind(summary_IHC, result)
    }
    DF <- summary_IHC
  }else if (cell.type == "OHC"){
    df_OHC <- rbind(Apical_OHC, Middle_OHC, Basal_OHC)
    summary_OHC <- vector()
    for(turn in c("apical", "middle", "basal")){
      df <- subset(df_OHC$ave, df_OHC$turn %in% turn)
      ave <- mean(df, na.rm=T)
      sd <- sd(df, na.rm=T)
      n= sum(!is.na(df))
      sem = sd/sqrt(n)
      result <- data.frame(turn, treatment, cell.type = "OHC", ave, sd, n,sem)
      summary_OHC <- rbind(summary_OHC, result)
    }
    DF <- summary_OHC
  }
  DF
}
