abs_turnsig <- function(treatment, cell.type){
  # apical: 0-30
  # middle: 35-70
  # basal: 75-100
  source("Abstract_calc.R")
  abscalc(treatment, 0,30,cell.type)
  apical=data.frame(Data,rep("Apical")) #Data is extracted from abscalc
  colnames(apical) <- c("intensity", "Turn")
  abscalc(treatment, 35,70,cell.type)
  middle=data.frame(Data,rep("Middle"))
  colnames(middle) <- c("intensity", "Turn")
  abscalc(treatment, 75,100,cell.type)
  basal=data.frame(Data,rep("Basal"))
  colnames(basal) <-c("intensity", "Turn")
  DataF=rbind(apical, middle,basal)	
  #wilcox.test(DataF$intensity ~ DataF$Turn, data=DataF) 
  kw <- kruskal.test(DataF$intensity ~ DataF$Turn, data=DataF)
  posthoc <- pairwise.wilcox.test(DataF$intensity, DataF$Turn, p.adjust.method ="bonferroni", exact=F)
  posthoc.data<-data.frame(posthoc$p.value)
  significance<-posthoc.data
  significance[significance<0.001]<-"***"
  significance[significance>=0.001 & significance < 0.01] <- "**"
  significance[significance>=0.01 & significance < 0.05] <- "*"
  significance[significance > 0.05] <- "-"
  significance
}
